namespace Messaging {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"Messaging.Print_System_doesUserExist", typeof(global::Messaging.Print_System_doesUserExist))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"Messaging.AD_returnOK", typeof(global::Messaging.AD_returnOK))]
    public sealed class Print_System_doesUserExist_To_AD : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 s1 userCSharp"" version=""1.0"" xmlns:s0=""http://Messaging.AD"" xmlns:ns0=""http://Messaging.AD_returnOK"" xmlns:s1=""http://Messaging.Print_System_doesUserExist"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s1:Print_System"" />
  </xsl:template>
  <xsl:template match=""/s1:Print_System"">
    <ns0:AD>
      <xsl:for-each select=""s0:AD/users/user"">
        <xsl:variable name=""var:v1"" select=""PW"" />
        <xsl:variable name=""var:v2"" select=""userCSharp:LogicalEq(string(../../../PW/text()) , string($var:v1))"" />
        <xsl:variable name=""var:v3"" select=""UID"" />
        <xsl:variable name=""var:v4"" select=""userCSharp:LogicalEq(string(../../../UID/text()) , string($var:v3))"" />
        <xsl:variable name=""var:v5"" select=""userCSharp:LogicalAnd(string($var:v2) , string($var:v4))"" />
        <xsl:if test=""string($var:v5)='true'"">
          <xsl:variable name=""var:v6"" select=""&quot;0&quot;"" />
          <response>
            <xsl:value-of select=""$var:v6"" />
          </response>
        </xsl:if>
      </xsl:for-each>
      <xsl:for-each select=""s0:AD/users/user"" />
    </ns0:AD>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public bool LogicalAnd(string param0, string param1)
{
	return ValToBool(param0) && ValToBool(param1);
	return false;
}


public bool LogicalEq(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 == d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) == 0;
	}
	return ret;
}


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool ValToBool(string val)
{
	if (val != null)
	{
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		val = val.Trim();
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		double d = 0;
		if (IsNumeric(val, ref d))
		{
			return (d > 0);
		}
	}
	return false;
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"Messaging.Print_System_doesUserExist";
        
        private const global::Messaging.Print_System_doesUserExist _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"Messaging.AD_returnOK";
        
        private const global::Messaging.AD_returnOK _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"Messaging.Print_System_doesUserExist";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"Messaging.AD_returnOK";
                return _TrgSchemas;
            }
        }
    }
}
