namespace PrintPaySysIntegration.Messaging
{
    using System;
    using System.Collections.Generic;
    using Microsoft.BizTalk.PipelineOM;
    using Microsoft.BizTalk.Component;
    using Microsoft.BizTalk.Component.Interop;
    
    
    public sealed class doesUserExist : Microsoft.BizTalk.PipelineOM.SendPipeline
    {
        
        private const string _strPipeline = "<?xml version=\"1.0\" encoding=\"utf-16\"?><Document xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instanc"+
"e\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" MajorVersion=\"1\" MinorVersion=\"0\">  <Description /> "+
" <CategoryId>8c6b051c-0ff5-4fc2-9ae5-5016cb726282</CategoryId>  <FriendlyName>Transmit</FriendlyName"+
">  <Stages>    <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"1\" Name=\"Pre-Assemble\" minO"+
"ccurs=\"0\" maxOccurs=\"-1\" execMethod=\"All\" stageId=\"9d0e4101-4cce-4536-83fa-4a5040674ad6\" />      <Co"+
"mponents />    </Stage>    <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"2\" Name=\"Assemb"+
"le\" minOccurs=\"0\" maxOccurs=\"1\" execMethod=\"All\" stageId=\"9d0e4107-4cce-4536-83fa-4a5040674ad6\" />  "+
"    <Components>        <Component>          <Name>Microsoft.BizTalk.Component.XmlAsmComp,Microsoft."+
"BizTalk.Pipeline.Components, Version=3.0.1.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35</Name"+
">          <ComponentName>XML assembler</ComponentName>          <Description>XML assembler componen"+
"t.</Description>          <Version>1.0</Version>          <Properties>            <Property Name=\"En"+
"velopeDocSpecNames\">              <Value xsi:type=\"xsd:string\">Messaging.Print_System_doesUserExist<"+
"/Value>            </Property>            <Property Name=\"EnvelopeSpecTargetNamespaces\">            "+
"  <Value xsi:type=\"xsd:string\">http://Messaging.Print_System_doesUserExist</Value>            </Prop"+
"erty>            <Property Name=\"DocumentSpecNames\">              <Value xsi:type=\"xsd:string\">Messa"+
"ging.Print_System_DB|Messaging.AD_DB</Value>            </Property>            <Property Name=\"Docum"+
"entSpecTargetNamespaces\">              <Value xsi:type=\"xsd:string\">http://Messaging.Print_System ht"+
"tp://Messaging.AD</Value>            </Property>            <Property Name=\"XmlAsmProcessingInstruct"+
"ions\" />            <Property Name=\"ProcessingInstructionsOptions\">              <Value xsi:type=\"xs"+
"d:int\">1</Value>            </Property>            <Property Name=\"ProcessingInstructionsScope\">    "+
"          <Value xsi:type=\"xsd:int\">0</Value>            </Property>            <Property Name=\"AddX"+
"mlDeclaration\">              <Value xsi:type=\"xsd:boolean\">true</Value>            </Property>      "+
"      <Property Name=\"TargetCharset\">              <Value xsi:type=\"xsd:string\" />            </Prop"+
"erty>            <Property Name=\"TargetCodePage\">              <Value xsi:type=\"xsd:int\">0</Value>  "+
"          </Property>            <Property Name=\"PreserveBom\">              <Value xsi:type=\"xsd:boo"+
"lean\">true</Value>            </Property>            <Property Name=\"HiddenProperties\">             "+
" <Value xsi:type=\"xsd:string\">EnvelopeSpecTargetNamespaces,DocumentSpecTargetNamespaces,TargetCodePa"+
"ge</Value>            </Property>          </Properties>          <CachedDisplayName>XML assembler</"+
"CachedDisplayName>          <CachedIsManaged>true</CachedIsManaged>        </Component>      </Compo"+
"nents>    </Stage>    <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"3\" Name=\"Encode\" min"+
"Occurs=\"0\" maxOccurs=\"-1\" execMethod=\"All\" stageId=\"9d0e4108-4cce-4536-83fa-4a5040674ad6\" />      <C"+
"omponents />    </Stage>  </Stages></Document>";
        
        private const string _versionDependentGuid = "52d9d03a-2e3a-4737-a107-89f5630c468c";
        
        public doesUserExist()
        {
            Microsoft.BizTalk.PipelineOM.Stage stage = this.AddStage(new System.Guid("9d0e4107-4cce-4536-83fa-4a5040674ad6"), Microsoft.BizTalk.PipelineOM.ExecutionMode.all);
            IBaseComponent comp0 = Microsoft.BizTalk.PipelineOM.PipelineManager.CreateComponent("Microsoft.BizTalk.Component.XmlAsmComp,Microsoft.BizTalk.Pipeline.Components, Version=3.0.1.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");;
            if (comp0 is IPersistPropertyBag)
            {
                string comp0XmlProperties = "<?xml version=\"1.0\" encoding=\"utf-16\"?><PropertyBag xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-inst"+
"ance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">  <Properties>    <Property Name=\"EnvelopeDocSpec"+
"Names\">      <Value xsi:type=\"xsd:string\">Messaging.Print_System_doesUserExist</Value>    </Property"+
">    <Property Name=\"EnvelopeSpecTargetNamespaces\">      <Value xsi:type=\"xsd:string\">http://Messagi"+
"ng.Print_System_doesUserExist</Value>    </Property>    <Property Name=\"DocumentSpecNames\">      <Va"+
"lue xsi:type=\"xsd:string\">Messaging.Print_System_DB|Messaging.AD_DB</Value>    </Property>    <Prope"+
"rty Name=\"DocumentSpecTargetNamespaces\">      <Value xsi:type=\"xsd:string\">http://Messaging.Print_Sy"+
"stem http://Messaging.AD</Value>    </Property>    <Property Name=\"XmlAsmProcessingInstructions\" /> "+
"   <Property Name=\"ProcessingInstructionsOptions\">      <Value xsi:type=\"xsd:int\">1</Value>    </Pro"+
"perty>    <Property Name=\"ProcessingInstructionsScope\">      <Value xsi:type=\"xsd:int\">0</Value>    "+
"</Property>    <Property Name=\"AddXmlDeclaration\">      <Value xsi:type=\"xsd:boolean\">true</Value>  "+
"  </Property>    <Property Name=\"TargetCharset\">      <Value xsi:type=\"xsd:string\" />    </Property>"+
"    <Property Name=\"TargetCodePage\">      <Value xsi:type=\"xsd:int\">0</Value>    </Property>    <Pro"+
"perty Name=\"PreserveBom\">      <Value xsi:type=\"xsd:boolean\">true</Value>    </Property>    <Propert"+
"y Name=\"HiddenProperties\">      <Value xsi:type=\"xsd:string\">EnvelopeSpecTargetNamespaces,DocumentSp"+
"ecTargetNamespaces,TargetCodePage</Value>    </Property>  </Properties></PropertyBag>";
                PropertyBag pb = PropertyBag.DeserializeFromXml(comp0XmlProperties);;
                ((IPersistPropertyBag)(comp0)).Load(pb, 0);
            }
            this.AddComponent(stage, comp0);
        }
        
        public override string XmlContent
        {
            get
            {
                return _strPipeline;
            }
        }
        
        public override System.Guid VersionDependentGuid
        {
            get
            {
                return new System.Guid(_versionDependentGuid);
            }
        }
    }
}
