namespace Messaging {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"PPSI_Biztalk.Messaging.Schema.Print_System_doesUserExist1", typeof(global::PPSI_Biztalk.Messaging.Schema.Print_System_doesUserExist1))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"PPSI_Biztalk.Messaging.Schema.AD_returnOK1", typeof(global::PPSI_Biztalk.Messaging.Schema.AD_returnOK1))]
    public sealed class Print_System_doesUserExist_To_AD : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 ScriptNS0 userCSharp"" version=""1.0"" xmlns:ns0=""http://PPSI_Biztalk.Messaging.AD_returnOK1"" xmlns:s0=""http://PPSI_Biztalk.Messaging.Print_System_doesUserExist1"" xmlns:ScriptNS0=""http://schemas.microsoft.com/BizTalk/2003/ScriptNS0"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:Print_System"" />
  </xsl:template>
  <xsl:template match=""/s0:Print_System"">
    <ns0:AD>
      <xsl:variable name=""var:v1"" select=""ScriptNS0:DBLookup(0 , string(UID/text()) , &quot;Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False; Initial Catalog=AD;Data Source=(local)&quot; , &quot;AD&quot; , &quot;uid&quot;)"" />
      <xsl:variable name=""var:v2"" select=""ScriptNS0:DBValueExtract(string($var:v1) , &quot;pw&quot;)"" />
      <xsl:variable name=""var:v3"" select=""userCSharp:LogicalEq(string(PW/text()) , string($var:v2))"" />
      <xsl:if test=""string($var:v3)='true'"">
        <xsl:variable name=""var:v4"" select=""&quot;0&quot;"" />
        <response>
          <xsl:value-of select=""$var:v4"" />
        </response>
      </xsl:if>
      <xsl:variable name=""var:v5"" select=""userCSharp:LogicalNot(string($var:v3))"" />
      <xsl:if test=""string($var:v5)='true'"">
        <xsl:variable name=""var:v6"" select=""&quot;1&quot;"" />
        <response>
          <xsl:value-of select=""$var:v6"" />
        </response>
      </xsl:if>
    </ns0:AD>
    <xsl:variable name=""var:v7"" select=""ScriptNS0:DBLookupShutdown()"" />
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public bool LogicalEq(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 == d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) == 0;
	}
	return ret;
}


public bool LogicalNot(string val)
{
	return !ValToBool(val);
}


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool ValToBool(string val)
{
	if (val != null)
	{
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		val = val.Trim();
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		double d = 0;
		if (IsNumeric(val, ref d))
		{
			return (d > 0);
		}
	}
	return false;
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects>
  <ExtensionObject Namespace=""http://schemas.microsoft.com/BizTalk/2003/ScriptNS0"" AssemblyName=""Microsoft.BizTalk.BaseFunctoids, Version=3.0.1.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"" ClassName=""Microsoft.BizTalk.BaseFunctoids.FunctoidScripts"" />
</ExtensionObjects>";
        
        private const string _strSrcSchemasList0 = @"PPSI_Biztalk.Messaging.Schema.Print_System_doesUserExist1";
        
        private const global::PPSI_Biztalk.Messaging.Schema.Print_System_doesUserExist1 _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"PPSI_Biztalk.Messaging.Schema.AD_returnOK1";
        
        private const global::PPSI_Biztalk.Messaging.Schema.AD_returnOK1 _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"PPSI_Biztalk.Messaging.Schema.Print_System_doesUserExist1";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"PPSI_Biztalk.Messaging.Schema.AD_returnOK1";
                return _TrgSchemas;
            }
        }
    }
}
