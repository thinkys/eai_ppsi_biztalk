namespace PPSI_Biztalk.Messaging.Map {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"PPSI_Biztalk.Messaging.Schema.MS_SQL", typeof(global::PPSI_Biztalk.Messaging.Schema.MS_SQL))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"PPSI_Biztalk.Messaging.Schema.Print_System.Print_System", typeof(global::PPSI_Biztalk.Messaging.Schema.Print_System.Print_System))]
    public sealed class MS_SQL_To_Print_System : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0"" version=""1.0"" xmlns:ns0=""http://PPSI_Biztalk.Messaging.Schema.PrintSystem"" xmlns:s0=""http://PPSI_Biztalk.Messaging.Schema.MS_SQL"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:MS_SQL"" />
  </xsl:template>
  <xsl:template match=""/s0:MS_SQL"">
    <ns0:Print_System>
      <card>
        <cardID>
          <xsl:value-of select=""cardID/text()"" />
        </cardID>
        <nbCopy>
          <xsl:value-of select=""nbCopy/text()"" />
        </nbCopy>
      </card>
    </ns0:Print_System>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"PPSI_Biztalk.Messaging.Schema.MS_SQL";
        
        private const global::PPSI_Biztalk.Messaging.Schema.MS_SQL _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"PPSI_Biztalk.Messaging.Schema.Print_System.Print_System";
        
        private const global::PPSI_Biztalk.Messaging.Schema.Print_System.Print_System _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"PPSI_Biztalk.Messaging.Schema.MS_SQL";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"PPSI_Biztalk.Messaging.Schema.Print_System.Print_System";
                return _TrgSchemas;
            }
        }
    }
}
