namespace Messaging {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"PPSI_Biztalk.Messaging.Schema.MS_SQL", typeof(global::PPSI_Biztalk.Messaging.Schema.MS_SQL))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"PPSI_Biztalk.Messaging.Schema.Print_System_returnOK.Print_System_returnOK", typeof(global::PPSI_Biztalk.Messaging.Schema.Print_System_returnOK.Print_System_returnOK))]
    public sealed class _old_MS_SQL_addQuotas_To_Print_System : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 ScriptNS0 userCSharp"" version=""1.0"" xmlns:s0=""http://PPSI_Biztalk.Messaging.Schema.MS_SQL"" xmlns:ns0=""http://PPSI_Biztalk.Messaging.Schema.Print_System_returnOK"" xmlns:ScriptNS0=""http://schemas.microsoft.com/BizTalk/2003/ScriptNS0"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:MS_SQL"" />
  </xsl:template>
  <xsl:template match=""/s0:MS_SQL"">
    <ns0:Print_System>
      <xsl:variable name=""var:v1"" select=""ScriptNS0:DBLookup(0 , string(cardID/text()) , &quot;Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False; Initial Catalog=PRINTSYSTEM;Data Source=(local)&quot; , &quot;PRINTSYSTEM&quot; , &quot;cardid&quot;)"" />
      <xsl:variable name=""var:v2"" select=""ScriptNS0:DBValueExtract(string($var:v1) , &quot;nbcopy&quot;)"" />
      <xsl:variable name=""var:v3"" select=""userCSharp:LogicalIsNumeric(string($var:v2))"" />
      <xsl:variable name=""var:v4"" select=""userCSharp:LogicalNot(string($var:v3))"" />
      <xsl:if test=""string($var:v4)='true'"">
        <xsl:variable name=""var:v5"" select=""&quot;1&quot;"" />
        <response>
          <xsl:value-of select=""$var:v5"" />
        </response>
      </xsl:if>
      <xsl:if test=""string($var:v3)='true'"">
        <xsl:variable name=""var:v6"" select=""&quot;0&quot;"" />
        <response>
          <xsl:value-of select=""$var:v6"" />
        </response>
      </xsl:if>
    </ns0:Print_System>
    <xsl:variable name=""var:v7"" select=""ScriptNS0:DBLookupShutdown()"" />
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public bool LogicalIsNumeric(string val)
{
	return IsNumeric(val);
}


public bool LogicalNot(string val)
{
	return !ValToBool(val);
}


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool ValToBool(string val)
{
	if (val != null)
	{
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		val = val.Trim();
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		double d = 0;
		if (IsNumeric(val, ref d))
		{
			return (d > 0);
		}
	}
	return false;
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects>
  <ExtensionObject Namespace=""http://schemas.microsoft.com/BizTalk/2003/ScriptNS0"" AssemblyName=""Microsoft.BizTalk.BaseFunctoids, Version=3.0.1.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"" ClassName=""Microsoft.BizTalk.BaseFunctoids.FunctoidScripts"" />
</ExtensionObjects>";
        
        private const string _strSrcSchemasList0 = @"PPSI_Biztalk.Messaging.Schema.MS_SQL";
        
        private const global::PPSI_Biztalk.Messaging.Schema.MS_SQL _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"PPSI_Biztalk.Messaging.Schema.Print_System_returnOK.Print_System_returnOK";
        
        private const global::PPSI_Biztalk.Messaging.Schema.Print_System_returnOK.Print_System_returnOK _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"PPSI_Biztalk.Messaging.Schema.MS_SQL";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"PPSI_Biztalk.Messaging.Schema.Print_System_returnOK.Print_System_returnOK";
                return _TrgSchemas;
            }
        }
    }
}
