namespace PPSI_Biztalk.Messaging.Map {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"PPSI_Biztalk.Messaging.Schema.PaymentDatabase", typeof(global::PPSI_Biztalk.Messaging.Schema.PaymentDatabase))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"PPSI_Biztalk.Messaging.Schema.SAP_HR", typeof(global::PPSI_Biztalk.Messaging.Schema.SAP_HR))]
    public sealed class PaymentDatabase_To_SAP_HR : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0"" version=""1.0"" xmlns:s0=""http://PPSI_Biztalk.Messaging.Schema.PaymentDatabase"" xmlns:ns0=""http://PPSI_Biztalk.Messaging.Schema.SAP_HR"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:PaymentDatabase"" />
  </xsl:template>
  <xsl:template match=""/s0:PaymentDatabase"">
    <ns0:SAP_HR>
      <account>
        <cardID>
          <xsl:value-of select=""cardID/text()"" />
        </cardID>
      </account>
    </ns0:SAP_HR>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"PPSI_Biztalk.Messaging.Schema.PaymentDatabase";
        
        private const global::PPSI_Biztalk.Messaging.Schema.PaymentDatabase _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"PPSI_Biztalk.Messaging.Schema.SAP_HR";
        
        private const global::PPSI_Biztalk.Messaging.Schema.SAP_HR _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"PPSI_Biztalk.Messaging.Schema.PaymentDatabase";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"PPSI_Biztalk.Messaging.Schema.SAP_HR";
                return _TrgSchemas;
            }
        }
    }
}
