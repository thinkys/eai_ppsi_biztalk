namespace Messaging {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"Messaging.MS_SQL_addQuotas", typeof(global::Messaging.MS_SQL_addQuotas))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"Messaging.Print_System_DB", typeof(global::Messaging.Print_System_DB))]
    public sealed class MS_SQL_addQuotas_To_Print_System : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 userCSharp"" version=""1.0"" xmlns:ns0=""http://Messaging.Print_System"" xmlns:s0=""http://Messaging.MS_SQL"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:MS_SQL"" />
  </xsl:template>
  <xsl:template match=""/s0:MS_SQL"">
    <ns0:Print_System>
      <cards>
        <xsl:for-each select=""ns0:Print_System/cards/card"">
          <xsl:variable name=""var:v1"" select=""cardID"" />
          <xsl:variable name=""var:v2"" select=""nbCopy"" />
          <card>
            <cardID>
              <xsl:value-of select=""$var:v1"" />
            </cardID>
            <xsl:variable name=""var:v3"" select=""userCSharp:addQuotas(string(../../../nbCopy/text()) , string($var:v1) , string($var:v2) , string(../../../cardID/text()))"" />
            <nbCopy>
              <xsl:value-of select=""$var:v3"" />
            </nbCopy>
          </card>
        </xsl:for-each>
      </cards>
    </ns0:Print_System>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
///*Uncomment the following code for a sample Inline C# function
//that concatenates two inputs. Change the number of parameters of
//this function to be equal to the number of inputs connected to this functoid.*/

public long addQuotas(long MSSQLnbCopy, long printCardID, long printNbCopy, long MSSQLCardID)
{
if(printCardID == MSSQLCardID){
return printNbCopy+MSSQLnbCopy;
}
else{
return printNbCopy;
}
}



]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"Messaging.MS_SQL_addQuotas";
        
        private const global::Messaging.MS_SQL_addQuotas _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"Messaging.Print_System_DB";
        
        private const global::Messaging.Print_System_DB _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"Messaging.MS_SQL_addQuotas";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"Messaging.Print_System_DB";
                return _TrgSchemas;
            }
        }
    }
}
