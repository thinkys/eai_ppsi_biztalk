namespace PrintPaySysIntegration.Messaging
{
    using System;
    using System.Collections.Generic;
    using Microsoft.BizTalk.PipelineOM;
    using Microsoft.BizTalk.Component;
    using Microsoft.BizTalk.Component.Interop;
    
    
    public sealed class getUID : Microsoft.BizTalk.PipelineOM.SendPipeline
    {
        
        private const string _strPipeline = "<?xml version=\"1.0\" encoding=\"utf-16\"?><Document xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instanc"+
"e\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" MajorVersion=\"1\" MinorVersion=\"0\">  <Description /> "+
" <CategoryId>8c6b051c-0ff5-4fc2-9ae5-5016cb726282</CategoryId>  <FriendlyName>Transmit</FriendlyName"+
">  <Stages>    <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"1\" Name=\"Pre-Assemble\" minO"+
"ccurs=\"0\" maxOccurs=\"-1\" execMethod=\"All\" stageId=\"9d0e4101-4cce-4536-83fa-4a5040674ad6\" />      <Co"+
"mponents />    </Stage>    <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"2\" Name=\"Assemb"+
"le\" minOccurs=\"0\" maxOccurs=\"1\" execMethod=\"All\" stageId=\"9d0e4107-4cce-4536-83fa-4a5040674ad6\" />  "+
"    <Components>        <Component>          <Name>Microsoft.BizTalk.Component.XmlAsmComp,Microsoft."+
"BizTalk.Pipeline.Components, Version=3.0.1.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35</Name"+
">          <ComponentName>XML assembler</ComponentName>          <Description>XML assembler componen"+
"t.</Description>          <Version>1.0</Version>          <Properties>            <Property Name=\"En"+
"velopeDocSpecNames\">              <Value xsi:type=\"xsd:string\">Messaging.PaymentDatabase_getUID</Val"+
"ue>            </Property>            <Property Name=\"EnvelopeSpecTargetNamespaces\">              <V"+
"alue xsi:type=\"xsd:string\">http://Messaging.PaymentDatabase</Value>            </Property>          "+
"  <Property Name=\"DocumentSpecNames\">              <Value xsi:type=\"xsd:string\">Messaging.PaymentDat"+
"abase|Messaging.SAP_HR_DB</Value>            </Property>            <Property Name=\"DocumentSpecTarg"+
"etNamespaces\">              <Value xsi:type=\"xsd:string\">http://Messaging.PaymentDatabase http://Mes"+
"saging.SAP_HR</Value>            </Property>            <Property Name=\"XmlAsmProcessingInstructions"+
"\" />            <Property Name=\"ProcessingInstructionsOptions\">              <Value xsi:type=\"xsd:in"+
"t\">1</Value>            </Property>            <Property Name=\"ProcessingInstructionsScope\">        "+
"      <Value xsi:type=\"xsd:int\">0</Value>            </Property>            <Property Name=\"AddXmlDe"+
"claration\">              <Value xsi:type=\"xsd:boolean\">true</Value>            </Property>          "+
"  <Property Name=\"TargetCharset\">              <Value xsi:type=\"xsd:string\" />            </Property"+
">            <Property Name=\"TargetCodePage\">              <Value xsi:type=\"xsd:int\">0</Value>      "+
"      </Property>            <Property Name=\"PreserveBom\">              <Value xsi:type=\"xsd:boolean"+
"\">true</Value>            </Property>            <Property Name=\"HiddenProperties\">              <Va"+
"lue xsi:type=\"xsd:string\">EnvelopeSpecTargetNamespaces,DocumentSpecTargetNamespaces,TargetCodePage</"+
"Value>            </Property>          </Properties>          <CachedDisplayName>XML assembler</Cach"+
"edDisplayName>          <CachedIsManaged>true</CachedIsManaged>        </Component>      </Component"+
"s>    </Stage>    <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"3\" Name=\"Encode\" minOccu"+
"rs=\"0\" maxOccurs=\"-1\" execMethod=\"All\" stageId=\"9d0e4108-4cce-4536-83fa-4a5040674ad6\" />      <Compo"+
"nents />    </Stage>  </Stages></Document>";
        
        private const string _versionDependentGuid = "8510227f-a1dc-40c6-a301-2db96efdda92";
        
        public getUID()
        {
            Microsoft.BizTalk.PipelineOM.Stage stage = this.AddStage(new System.Guid("9d0e4107-4cce-4536-83fa-4a5040674ad6"), Microsoft.BizTalk.PipelineOM.ExecutionMode.all);
            IBaseComponent comp0 = Microsoft.BizTalk.PipelineOM.PipelineManager.CreateComponent("Microsoft.BizTalk.Component.XmlAsmComp,Microsoft.BizTalk.Pipeline.Components, Version=3.0.1.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");;
            if (comp0 is IPersistPropertyBag)
            {
                string comp0XmlProperties = "<?xml version=\"1.0\" encoding=\"utf-16\"?><PropertyBag xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-inst"+
"ance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">  <Properties>    <Property Name=\"EnvelopeDocSpec"+
"Names\">      <Value xsi:type=\"xsd:string\">Messaging.PaymentDatabase_getUID</Value>    </Property>   "+
" <Property Name=\"EnvelopeSpecTargetNamespaces\">      <Value xsi:type=\"xsd:string\">http://Messaging.P"+
"aymentDatabase</Value>    </Property>    <Property Name=\"DocumentSpecNames\">      <Value xsi:type=\"x"+
"sd:string\">Messaging.PaymentDatabase|Messaging.SAP_HR_DB</Value>    </Property>    <Property Name=\"D"+
"ocumentSpecTargetNamespaces\">      <Value xsi:type=\"xsd:string\">http://Messaging.PaymentDatabase htt"+
"p://Messaging.SAP_HR</Value>    </Property>    <Property Name=\"XmlAsmProcessingInstructions\" />    <"+
"Property Name=\"ProcessingInstructionsOptions\">      <Value xsi:type=\"xsd:int\">1</Value>    </Propert"+
"y>    <Property Name=\"ProcessingInstructionsScope\">      <Value xsi:type=\"xsd:int\">0</Value>    </Pr"+
"operty>    <Property Name=\"AddXmlDeclaration\">      <Value xsi:type=\"xsd:boolean\">true</Value>    </"+
"Property>    <Property Name=\"TargetCharset\">      <Value xsi:type=\"xsd:string\" />    </Property>    "+
"<Property Name=\"TargetCodePage\">      <Value xsi:type=\"xsd:int\">0</Value>    </Property>    <Propert"+
"y Name=\"PreserveBom\">      <Value xsi:type=\"xsd:boolean\">true</Value>    </Property>    <Property Na"+
"me=\"HiddenProperties\">      <Value xsi:type=\"xsd:string\">EnvelopeSpecTargetNamespaces,DocumentSpecTa"+
"rgetNamespaces,TargetCodePage</Value>    </Property>  </Properties></PropertyBag>";
                PropertyBag pb = PropertyBag.DeserializeFromXml(comp0XmlProperties);;
                ((IPersistPropertyBag)(comp0)).Load(pb, 0);
            }
            this.AddComponent(stage, comp0);
        }
        
        public override string XmlContent
        {
            get
            {
                return _strPipeline;
            }
        }
        
        public override System.Guid VersionDependentGuid
        {
            get
            {
                return new System.Guid(_versionDependentGuid);
            }
        }
    }
}
