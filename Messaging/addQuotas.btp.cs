namespace PrintPaySysIntegration.Messaging
{
    using System;
    using System.Collections.Generic;
    using Microsoft.BizTalk.PipelineOM;
    using Microsoft.BizTalk.Component;
    using Microsoft.BizTalk.Component.Interop;
    
    
    public sealed class addQuotas : Microsoft.BizTalk.PipelineOM.SendPipeline
    {
        
        private const string _strPipeline = "<?xml version=\"1.0\" encoding=\"utf-16\"?><Document xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instanc"+
"e\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" MajorVersion=\"1\" MinorVersion=\"0\">  <Description /> "+
" <CategoryId>8c6b051c-0ff5-4fc2-9ae5-5016cb726282</CategoryId>  <FriendlyName>Transmit</FriendlyName"+
">  <Stages>    <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"1\" Name=\"Pre-Assemble\" minO"+
"ccurs=\"0\" maxOccurs=\"-1\" execMethod=\"All\" stageId=\"9d0e4101-4cce-4536-83fa-4a5040674ad6\" />      <Co"+
"mponents />    </Stage>    <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"2\" Name=\"Assemb"+
"le\" minOccurs=\"0\" maxOccurs=\"1\" execMethod=\"All\" stageId=\"9d0e4107-4cce-4536-83fa-4a5040674ad6\" />  "+
"    <Components>        <Component>          <Name>Microsoft.BizTalk.Component.XmlAsmComp,Microsoft."+
"BizTalk.Pipeline.Components, Version=3.0.1.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35</Name"+
">          <ComponentName>XML assembler</ComponentName>          <Description>XML assembler componen"+
"t.</Description>          <Version>1.0</Version>          <Properties>            <Property Name=\"En"+
"velopeDocSpecNames\">              <Value xsi:type=\"xsd:string\">Messaging.MS_SQL_addQuotas</Value>   "+
"         </Property>            <Property Name=\"EnvelopeSpecTargetNamespaces\">              <Value x"+
"si:type=\"xsd:string\">http://Messaging.MS_SQL</Value>            </Property>            <Property Nam"+
"e=\"DocumentSpecNames\">              <Value xsi:type=\"xsd:string\">Messaging.MS_SQL_DB|Messaging.Print"+
"_System_DB</Value>            </Property>            <Property Name=\"DocumentSpecTargetNamespaces\"> "+
"             <Value xsi:type=\"xsd:string\">http://Messaging.MS_SQL_DB http://Messaging.Print_System</"+
"Value>            </Property>            <Property Name=\"XmlAsmProcessingInstructions\" />           "+
" <Property Name=\"ProcessingInstructionsOptions\">              <Value xsi:type=\"xsd:int\">0</Value>   "+
"         </Property>            <Property Name=\"ProcessingInstructionsScope\">              <Value xs"+
"i:type=\"xsd:int\">0</Value>            </Property>            <Property Name=\"AddXmlDeclaration\">    "+
"          <Value xsi:type=\"xsd:boolean\">true</Value>            </Property>            <Property Nam"+
"e=\"TargetCharset\">              <Value xsi:type=\"xsd:string\" />            </Property>            <P"+
"roperty Name=\"TargetCodePage\">              <Value xsi:type=\"xsd:int\">0</Value>            </Propert"+
"y>            <Property Name=\"PreserveBom\">              <Value xsi:type=\"xsd:boolean\">true</Value> "+
"           </Property>            <Property Name=\"HiddenProperties\">              <Value xsi:type=\"x"+
"sd:string\">EnvelopeSpecTargetNamespaces,DocumentSpecTargetNamespaces,TargetCodePage</Value>         "+
"   </Property>          </Properties>          <CachedDisplayName>XML assembler</CachedDisplayName> "+
"         <CachedIsManaged>true</CachedIsManaged>        </Component>      </Components>    </Stage> "+
"   <Stage>      <PolicyFileStage _locAttrData=\"Name\" _locID=\"3\" Name=\"Encode\" minOccurs=\"0\" maxOccur"+
"s=\"-1\" execMethod=\"All\" stageId=\"9d0e4108-4cce-4536-83fa-4a5040674ad6\" />      <Components />    </S"+
"tage>  </Stages></Document>";
        
        private const string _versionDependentGuid = "acd5a833-ef67-4301-9a94-e59047acd773";
        
        public addQuotas()
        {
            Microsoft.BizTalk.PipelineOM.Stage stage = this.AddStage(new System.Guid("9d0e4107-4cce-4536-83fa-4a5040674ad6"), Microsoft.BizTalk.PipelineOM.ExecutionMode.all);
            IBaseComponent comp0 = Microsoft.BizTalk.PipelineOM.PipelineManager.CreateComponent("Microsoft.BizTalk.Component.XmlAsmComp,Microsoft.BizTalk.Pipeline.Components, Version=3.0.1.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");;
            if (comp0 is IPersistPropertyBag)
            {
                string comp0XmlProperties = "<?xml version=\"1.0\" encoding=\"utf-16\"?><PropertyBag xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-inst"+
"ance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">  <Properties>    <Property Name=\"EnvelopeDocSpec"+
"Names\">      <Value xsi:type=\"xsd:string\">Messaging.MS_SQL_addQuotas</Value>    </Property>    <Prop"+
"erty Name=\"EnvelopeSpecTargetNamespaces\">      <Value xsi:type=\"xsd:string\">http://Messaging.MS_SQL<"+
"/Value>    </Property>    <Property Name=\"DocumentSpecNames\">      <Value xsi:type=\"xsd:string\">Mess"+
"aging.MS_SQL_DB|Messaging.Print_System_DB</Value>    </Property>    <Property Name=\"DocumentSpecTarg"+
"etNamespaces\">      <Value xsi:type=\"xsd:string\">http://Messaging.MS_SQL_DB http://Messaging.Print_S"+
"ystem</Value>    </Property>    <Property Name=\"XmlAsmProcessingInstructions\" />    <Property Name=\""+
"ProcessingInstructionsOptions\">      <Value xsi:type=\"xsd:int\">0</Value>    </Property>    <Property"+
" Name=\"ProcessingInstructionsScope\">      <Value xsi:type=\"xsd:int\">0</Value>    </Property>    <Pro"+
"perty Name=\"AddXmlDeclaration\">      <Value xsi:type=\"xsd:boolean\">true</Value>    </Property>    <P"+
"roperty Name=\"TargetCharset\">      <Value xsi:type=\"xsd:string\" />    </Property>    <Property Name="+
"\"TargetCodePage\">      <Value xsi:type=\"xsd:int\">0</Value>    </Property>    <Property Name=\"Preserv"+
"eBom\">      <Value xsi:type=\"xsd:boolean\">true</Value>    </Property>    <Property Name=\"HiddenPrope"+
"rties\">      <Value xsi:type=\"xsd:string\">EnvelopeSpecTargetNamespaces,DocumentSpecTargetNamespaces,"+
"TargetCodePage</Value>    </Property>  </Properties></PropertyBag>";
                PropertyBag pb = PropertyBag.DeserializeFromXml(comp0XmlProperties);;
                ((IPersistPropertyBag)(comp0)).Load(pb, 0);
            }
            this.AddComponent(stage, comp0);
        }
        
        public override string XmlContent
        {
            get
            {
                return _strPipeline;
            }
        }
        
        public override System.Guid VersionDependentGuid
        {
            get
            {
                return new System.Guid(_versionDependentGuid);
            }
        }
    }
}
