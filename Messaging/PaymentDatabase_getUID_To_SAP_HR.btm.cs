namespace Messaging {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"Messaging.PaymentDatabase_getUID", typeof(global::Messaging.PaymentDatabase_getUID))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"Messaging.SAP_HR_returnUID", typeof(global::Messaging.SAP_HR_returnUID))]
    public sealed class PaymentDatabase_getUID_To_SAP_HR : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s1 s0 userCSharp"" version=""1.0"" xmlns:ns0=""http://Messaging.SAP_HR_returnUID"" xmlns:s0=""http://Messaging.SAP_HR"" xmlns:s1=""http://Messaging.PaymentDatabase"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s1:PaymentDatabase"" />
  </xsl:template>
  <xsl:template match=""/s1:PaymentDatabase"">
    <xsl:for-each select=""s0:SAP_HR"">
      <ns0:SAP_HR>
        <xsl:for-each select=""accounts/account"">
          <xsl:variable name=""var:v1"" select=""cardID"" />
          <xsl:variable name=""var:v2"" select=""userCSharp:LogicalEq(string(../../../cardID/text()) , string($var:v1))"" />
          <xsl:if test=""string($var:v2)='true'"">
            <xsl:variable name=""var:v3"" select=""UID"" />
            <UID>
              <xsl:value-of select=""$var:v3"" />
            </UID>
          </xsl:if>
        </xsl:for-each>
        <xsl:for-each select=""accounts/account"" />
      </ns0:SAP_HR>
    </xsl:for-each>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public bool LogicalEq(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 == d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) == 0;
	}
	return ret;
}


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"Messaging.PaymentDatabase_getUID";
        
        private const global::Messaging.PaymentDatabase_getUID _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"Messaging.SAP_HR_returnUID";
        
        private const global::Messaging.SAP_HR_returnUID _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"Messaging.PaymentDatabase_getUID";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"Messaging.SAP_HR_returnUID";
                return _TrgSchemas;
            }
        }
    }
}
