namespace PPSI_Biztalk.Messaging.Schema {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://PPSI_Biztalk.Messaging.MS_SQL_addQuotas",@"MS_SQL")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"MS_SQL"})]
    public sealed class MS_SQL_addQuotas1 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://PPSI_Biztalk.Messaging.MS_SQL_addQuotas"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" targetNamespace=""http://PPSI_Biztalk.Messaging.MS_SQL_addQuotas"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:element name=""MS_SQL"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""UID"" type=""xs:unsignedShort"" />
        <xs:element name=""cardID"" type=""xs:unsignedShort"" />
        <xs:element name=""nbCopy"" type=""xs:unsignedByte"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public MS_SQL_addQuotas1() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "MS_SQL";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
