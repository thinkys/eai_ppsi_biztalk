namespace PPSI_Biztalk.Messaging.Schema {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://PPSI_Biztalk.Messaging.Schema.PaymentDatabase",@"PaymentDatabase")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"PaymentDatabase"})]
    public sealed class PaymentDatabase : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://PPSI_Biztalk.Messaging.Schema.PaymentDatabase"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" targetNamespace=""http://PPSI_Biztalk.Messaging.Schema.PaymentDatabase"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:element name=""PaymentDatabase"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""cardID"" type=""xs:unsignedShort"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public PaymentDatabase() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "PaymentDatabase";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
